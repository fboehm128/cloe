Package: cloe
Title: CLOnality and Evolution
Version: 0.9.7
Date: 2016-03-16
Authors@R: person("Francesco", "Marass",
  email="francesco.marass@cruk.cam.ac.uk", role=c("aut", "cre"))
Author: Francesco Marass <francesco.marass@cruk.cam.ac.uk>
Maintainer: Francesco Marass <francesco.marass@cruk.cam.ac.uk>
Description: Tumour clonality deconvolution from multiple tumour samples of the
    same patient. cloe uses a phylogenetic latent feature model to identify
    patterns (translated as clones) in the data. cloe thus describes a set of
    samples in terms of clones/patterns that are found across samples, in at
    least one sample.
Depends:
    R (>= 3.1.2),
    R6,
    methods
Imports:
    compiler,
    digest,
    ggplot2,
    igraph,
    RColorBrewer,
    reshape2
Suggests:
    gridExtra,
    knitr,
    rmarkdown
License: GPL-3 | file LICENSE
BugReports: https://bitbucket.org/fm361/cloe
RoxygenNote: 5.0.1
VignetteBuilder: knitr
