#' Estimate parameters
#' 
#' Summarises the MCMCMC output to infer the parameters of interest: genotypes,
#' their phylogeny, and clonal fractions.
#' 
#' @param mcmc_object a \code{cloe_mcmc} object
#' @param method a summary method. Currently only 'map' (maximum a posteriori)
#'   is available.
#' @param ... additional method-specific parameters
#' @return a \code{cloe_summary} object with inferred parameters
#' @seealso \code{\link{cloe_mcmc}} for \code{mcmc_object},
#'   \code{\link{cloe_summary}} for the return value, and
#'   \code{\link{summarise_map}} for the MAP summary
#' @export
summarise <- function(mcmc_object, method="map", ...){
  stopifnot("cloe_mcmc" %in% class(mcmc_object))

  if(method == "map"){
    return(summarise_map(mcmc_object=mcmc_object, ...))
  }else{
    stop("Method '", method, "' is not recognised.")
  }
}


summarise_map_r <- function(mcmc_object, burn=0.5, thin=1L,
                            solutions=5L, verbose=TRUE, ...){
  # check arguments
  stopifnot("cloe_mcmc" %in% class(mcmc_object))
  input <- mcmc_object$get_input()

  iterations <- mcmc_object$get_params(v="iterations")
  method <- mcmc_object$get_method()

  burn <- as.numeric(burn)
  if(is.na(burn) || burn < 0 || burn >= 1){
    stop("'burn' must be in [0, 1).")
  }

  thin <- as.integer(thin)
  if(is.na(thin) || thin < 1 ||
    (burn > 0 && thin > iterations * burn)){
    warning("'thin' must be an integer greater than 0 and ",
      "less than the number of post-burn-in iterations")
    thin <- 1L
  }

  solutions <- as.integer(solutions)
  if(is.na(solutions) || solutions < 1L || solutions > 100L){
    solutions <- 5L
    warning("'solutions' must be an integer between 1 and 100. ",
      "Reset to default (", solutions, ")")
  }

  if(!is.logical(verbose)){
    verbose <- TRUE
  }

  if(verbose) message("Returning top ", solutions, " parameters by their ",
      "posterior probability")

  data_type <- mcmc_object$get_type()
  tree_present <- TRUE

  # burn-in samples
  if(burn == 0){
    iter_start <- 1
  }else{
    iter_start <- max(1, round(burn * iterations) + 1)
  }
  iter_sel <- iter_start:iterations
  # thin samples
  if(thin > 1L){
    iter_sel <- iter_sel[seq(1, length(iter_sel), by=thin)]
  }
  iter_n <- length(iter_sel)

  # get parameter estimates by posterior (MAP)
  trace_likelihood <- mcmc_object$get_traces(v="loglikelihood", i=iter_sel)
  trace_posterior  <- mcmc_object$get_traces(v="logposterior", i=iter_sel)
  top_solutions_i  <- order(trace_posterior, decreasing=TRUE)[1:solutions]

  # here are the parameters
  if(method == "cnn"){
    top_zs <- mcmc_object$get_traces(v="Z", i=iter_sel[top_solutions_i])
    top_fs <- mcmc_object$get_traces(v="F", i=iter_sel[top_solutions_i])
  }else{
    stop("Method of cloe_mcmc object is not recognised")
  }

  # Remove replicate clones and merge their clonal fractions, convert to
  # matrices
  for(i in 1:solutions){
    this_history_dup <- duplicated(top_zs[[i]])
    if(any(this_history_dup)){
      # if something is duplicated, just go through non-duplicated clones and
      # merge them with their copies, if any
      for(k in which(!this_history_dup)){
        # clonal fractions of k are the sums of the clonal fractions of all
        # copies of k
        w_i <- which(top_zs[[i]] == top_zs[[i]][k])
        if(length(w_i) > 1){
          top_fs[[i]][k, ] <- colSums(top_fs[[i]][w_i, ])
        }
      }
      top_zs[[i]] <- top_zs[[i]][!this_history_dup]
      top_fs[[i]] <- top_fs[[i]][!this_history_dup, ]
    }
  }

  # turn zs to binary matrices
  top_zs_mat <- lapply(top_zs, vapply, function(x) id2bits(x, input$get_j()), rep(0L, input$get_j()))

  # rename dimensions
  for(i in 1:solutions){
    # overwrite clone names, if Z was named and fixed
    tmp <- top_zs[[i]]
    names(tmp) <- NULL
    colnames(top_zs_mat[[i]]) <- tmp

    rownames(top_zs_mat[[i]]) <- rownames(input$get_mutant_reads())
  }

  # now deal with trees
  if(tree_present){
    # this does not mind whether we have removed some duplicates, it uses all
    top_trees <- mcmc_object$get_traces(v="tree", i=iter_sel[top_solutions_i])
    T_inferred <- vector("list", solutions)
    for(i in 1:solutions){
      # exclude normal
      T_inferred[[i]] <- matrix("", nrow=mcmc_object$get_params(v="K")[[1]] - 1L, ncol=2L)
      T_inferred[[i]][, 1] <- top_trees[[i]][-1]
      T_inferred[[i]][, 2] <- names(top_trees[[i]])[-1]
    }
  }

  # rename clones
  for(i in 1:solutions){
    prev_names <- unique(top_zs[[i]])
    colnames(top_zs_mat[[i]]) <- rownames(top_fs[[i]]) <- paste0("C",
      vapply(colnames(top_zs_mat[[i]]), function(x) which(prev_names == x), 1L))
    if(tree_present){
      T_inferred[[i]] <- apply(T_inferred[[i]], 1:2, function(x) paste0("C", which(prev_names == x)))
    }
  }

  ret <- cloe_summary$new(
    input=input,
    type=data_type,
    model=mcmc_object$get_method(),
    method="map",
    params=list(
      burn=burn,
      thin=thin,
      solutions=solutions,
      K=mcmc_object$get_params(v="K")[[1]],
      K0=mcmc_object$get_params(v="K0")[[1]]
    ),
    genotypes=top_zs_mat,
    cfs=top_fs,
    trees=if(tree_present){ T_inferred }else{ NULL },
    loglikelihoods=trace_likelihood[top_solutions_i],
    logposteriors=trace_posterior[top_solutions_i],
    postprocessed=FALSE
  )
}
#' Estimate parameters by MAP
#' 
#' This method returns the maximum a posteriori (MAP) estimate of genotypes,
#' clonal fractions, and their phylogeny, according to the log-posterior
#' probabilities. Multiple solutions can be returned, in decreasing order of
#' log-posterior probability.
#' 
#' @param mcmc_object a \code{cloe_mcmc} object
#' @param burn the proportion of initial MCMC samples to discard as burn-in
#' @param thin the thinning interval. This values needs to be an integer. A
#'   value of \code{n} means that every \code{n^th} sample will be kept. One
#'   (1L) means that no thinning will be applied.
#' @param solutions the number of solutions to return. By default this is
#'   \code{5L}.
#' @param verbose whether to output messages
#' @param ... any additional parameters will be ignored
#' @return a \code{cloe_summary} object with inferred parameters for as many
#'   solutions as specified by the argument \code{solutions}
#' @seealso \code{\link{cloe_input}} for the input object,
#'   \code{\link{cloe_mcmc}} for \code{mcmc_object},
#'   \code{\link{cloe_summary}} for the return value, and
#'   \code{\link{summarise}} for the generic summary function
#' @export
summarise_map <- compiler::cmpfun(summarise_map_r)


#' Select best model from a list of cloe_summary objects
#'
#' Because cloe's model is finite, it must be run various times with different
#' model sizes (K, the number of clones). Choosing between solutions can be
#' done by evaluating the goodness of fit of the model through the
#' log-likelihood values, and accounting to a certain degree for the model
#' complexity through the log-posterior probabilities. This function helps
#' you accomplish this.
#' 
#' A number (chosen with the \code{solutions} argument) of parameter sets is
#' returned, sorted by decreasing log-posterior probability. We recommend to
#' choose the model with the highest log-posterior probability. In case several
#' models of different sizes have comparable log-posterior values, choose the
#' best fit to the data (highest log-likelihood).
#' 
#' @param l a list of \code{cloe_summary} objects referring to the same
#'   \code{cloe_input} input object
#' @param solutions the number of solutions to return (sorted by descending
#'   log-posterior value)
#' @param plot whether to plot log-likelihoods, log-posteriors of all models
#'   for manual/visual selection
#' @param path the path where to output the plots
#' @return a list of \code{cloe_summary} objects with the selected models
#' @importFrom ggplot2 aes geom_jitter ggplot ggsave ggtitle
#'   scale_colour_discrete xlab ylab
#' @export 
select_model <- function(l, solutions=1L, plot=TRUE, path=getwd()){
  if(!is.list(l) ||
     any(! sapply(l, function(x) "cloe_summary" %in% class(x)))){
    stop("'l' must be a list of cloe_summary objects")
  }

  # check that all inputs are the same
  input_digest <- digest::digest(l[[1]]$get_input(), algo="md5")
  if(any(! sapply(l, function(x) digest::digest(x$get_input(), algo="md5") == input_digest))){
    stop("All cloe_summary objects must be of the same cloe_input object")
  }

  # check that all models are the same
  sampling_model <- l[[1]]$get_model()
  if(any(! sapply(l, function(x) x$get_model() == sampling_model))){
    stop("All cloe_summary objects must come from the same sampling model")
  }

  solns <- sapply(lapply(l, function(x) x$get_logposteriors()), length)
  lls   <- unlist(lapply(l, function(x) x$get_loglikelihoods()))
  lps   <- unlist(lapply(l, function(x) x$get_logposteriors()))
  # k_run <- rep(sapply(l, function(x) x$get_params("K")[[1]]), solns)
  if(is.list(l[[1]]$get_params("K"))){
    k_run <- rep(sapply(l, function(x) x$get_params("K")[[1]]), solns)
  }else{
    k_run <- rep(sapply(l, function(x) x$get_params("K")), solns)
  }
  k_inf <- unlist(lapply(l, function(x) lapply(x$get_genotypes(), ncol)))

  if(plot){
    df <- data.frame(Loglikelihood=lls,   Logposterior=lps,
                     K_run=factor(k_run), K_inferred=factor(k_inf))

    gg <- ggplot(df, aes(K_run, Logposterior)) +
      geom_jitter(aes(colour=K_inferred), height=0, width=0.2) +
      xlab("K run") + ylab("Log-posterior") +
      scale_colour_discrete(name="K inferred") +
      ggtitle("Log-posteriors")
    ggsave(file.path(path, "model_selection_lp.pdf"), plot=gg, width=8, height=6, units="in")

    gg <- ggplot(df, aes(K_run, Loglikelihood)) +
      geom_jitter(aes(colour=K_inferred), height=0, width=0.2) +
      xlab("K run") + ylab("Log-likelihood") +
      scale_colour_discrete(name="K inferred") +
      ggtitle("Log-likelihoods")
    ggsave(file.path(path, "model_selection_ll.pdf"), plot=gg, width=8, height=6, units="in")
  }

  lps_order <- order(lps, decreasing=TRUE)
  solns_cs <- cumsum(solns)

  ret <- list()

  for(s in 1:solutions){
    i <- which(lps_order[s] <= solns_cs)[1]
    j <- lps_order[s] - (solns_cs - solns)[i]

    params_s <- l[[i]]$get_params()
    params_s[["solutions"]] <- 1L

    ret[[s]] <- cloe_summary$new(
        input=l[[i]]$get_input(),
        type=l[[i]]$get_type(),
        model=l[[i]]$get_model(),
        method=l[[i]]$get_method(),
        params=params_s,
        genotypes=l[[i]]$get_genotypes()[j],
        cfs=l[[i]]$get_clonal_fractions()[j],
        trees=l[[i]]$get_phylogenies()[j],
        loglikelihoods=l[[i]]$get_loglikelihoods()[j],
        logposteriors=l[[i]]$get_logposteriors()[j],
        postprocessed=l[[i]]$is_postprocessed()
      )
  }

  ret
}




