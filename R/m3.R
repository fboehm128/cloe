iterate_r <- function(vars, obs_data, params, tau=1){
  ## update Z and tree
  if(!params[["fix_z_and_tree"]]){
    # only update Z, then tree
    mh_update <- update_z_mh(vars=vars, obs_data=obs_data, params=params,
        iterations=4L, tau=tau)
    vars[["Z"]] <- mh_update[[1]]
    vars[["mh_z_accepted"]] <- mh_update[[2]]
    vars[["mh_z_trials"]]   <- mh_update[[3]]

    # This update may shuffle the cols of Z and rows of F.
    tree_update <- update_tree(vars=vars, obs_data=obs_data, params=params,
        tau=tau)
    vars[["tree"]] <- tree_update[[1]]
    vars[["Z"]] <- tree_update[[2]]
    vars[["F"]] <- tree_update[[3]]
    vars[["subtree_changes"]] <- tree_update[[4]]
    vars[["sibling_swaps"]] <- tree_update[[5]]
    vars[["parent_swaps"]] <- tree_update[[5]]

    # update list of ancestors
    if(params[["isa"]]){
      for(k in 3:params[["K"]]){
        vars[["ancestors"]][[k]] <- ancestors(tree=vars[["tree"]], node=k)
      }
    }
  }


  ## update F
  if(!params[["fix_f"]]){
    mh_update <- update_f(vars=vars, obs_data=obs_data, params=params,
        iterations=2L, tau=tau)
    vars[["F"]] <- mh_update[[1]]
    vars[["mh_f_accepted"]] <- mh_update[[2]]
    vars[["mh_f_trials"]]   <- mh_update[[3]]
  }


  ## update gammas
  if(!params[["fix_gammas"]]){
    mh_update <- update_gammas_dirichlet(vars=vars, obs_data=obs_data,
        params=params, tau=tau)
    vars[["gammas"]] <- mh_update[[1]]
    vars[["mh_gammas_accepted"]] <- mh_update[[2]]
    vars[["mh_gammas_trials"]]   <- mh_update[[3]]
  }


  ## update s
  if(!params[["fix_s"]]){
    mh_update <- update_s(vars=vars, obs_data=obs_data, params=params, tau=tau)
    vars[["s"]] <- mh_update[1]
    vars[["mh_s_accepted"]] <- mh_update[2]
    vars[["mh_s_trials"]]   <- mh_update[3]
  }

  vars
}
#' One iteration of MCMC for the CNN model
#'
#' This function corresponds to a single \emph{scan} of the sampler for
#' copy-number neutral mutations. The scan updates all parameters (\code{Z},
#' \code{tree}, \code{F}, \code{gammas} and \code{s}). These variable
#' parameters are contained in the named list \code{vars}, which is returned
#' (updated) at the end of the function.
#'
#' In MCMCMC, \code{tau} is the inverse temperature parameter and allows the
#' scan to be performed in a flattened posterior probability space.
#'
#' This function should not be called manually. No input sanitation is
#' performed.
#'
#' @param vars A named list containing quantities that are changed in updating
#'   all model parameters. This includes both parameters and update statistics.
#'
#'   Required entries are \code{Z}, \code{mh_z_trials}, \code{mh_z_accepted}, 
#'   \code{tree}, \code{subtree_changes}, \code{sibling_swaps}, \code{F},
#'   \code{mh_f_trials}, \code{mh_f_accepted}, \code{gammas},
#'   \code{mh_gammas_accepted}, \code{mh_gammas_trials}, \code{s},
#'   \code{mh_s_accepted}, \code{mh_s_trials}. 
#' @param obs_data A named list containing quantities that are observed.
#'   Required entries are \code{X}, \code{Y}, \code{J}, \code{T}.
#' @param params A named list containing fixed parameters of the MCMC algorithm.
#'   Required entries are:
#'   \describe{
#'     \item{K}{the number of clones in the model}
#'     \item{lmu}{log-probability of a mutation}
#'     \item{lmu1m}{log-probability of not mutating}
#'     \item{lrho}{log-probability of reversion}
#'     \item{lrho1m}{log-probability of no reversion}
#'     \item{isa}{whether to run ISA computations}
#'     \item{lnu}{log-factor for mutations that respect ISA }
#'     \item{lnu1m}{log-factor for mutations that violate ISA}
#'     \item{e}{the noise rate (probability of observing a mutant read by
#'       error)}
#'     \item{e1m}{one minus the noise rate}
#'     \item{theta}{probability of changing a clone's genotype in the proposal
#'       distribution}
#'     \item{fix_z_and_tree}{whether the genotypes matrix and the phylogeny are
#'       fixed}
#'     \item{fix_f}{whether the clonal fractions matrix is fixed}
#'     \item{fix_gammas}{whether the Dirichlet parameters are fixed}
#'     \item{fix_s}{whether the Beta-binomial overdispersion parameter is fixed}
#'     \item{f_scaling}{the scaling factor for the Dirichlet proposal. The new
#'       vector is obtained by the product of
#'       \code{f_scaling * old_vector + f_bias}}
#'     \item{f_bias}{the bias for the Dirichlet proposal. The new vector is
#'       obtained by the product of \code{f_scaling * old_vector + f_bias}}
#'     \item{gamma_shape}{the shape parameter of the prior Gamma distribution
#'       of the gammas (symmetric Dirichlet parameters)}
#'     \item{gamma_rate}{the rate parameter of the prior Gamma distribution of
#'       the gammas (symmetric Dirichlet parameters)}
#'     \item{mh_gamma_sd}{the standard deviation of the Gaussian proposal of
#'       \code{gammas}}
#'     \item{s_shape}{the shape parameter of the prior Gamma distribution of
#'       the overdispersion parameter}
#'     \item{s_rate}{the rate parameter of the prior Gamma distribution of
#'       the overdispersion parameter}
#'     \item{mh_s_sd}{the standard deviation of the Gaussian proposal for
#'       \code{s}}
#'     \item{lpriors_tree}{a list of length \code{K} with the prior
#'       probabilities for each parent assignment}
#'     \item{ca}{the vector \code{K:1} to save operations in \code{\link{mrca}}}
#'   }
#' @param tau inverse temperature parameter to flatten the posterior
#' @return a named list of the same for as \code{vars} but with updated
#'   parameters following a round of updates for each non-fixed parameter.
iterate <- compiler::cmpfun(iterate_r)
