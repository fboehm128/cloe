#' MCMCMC sampler
#' 
#' Starts cloe's MCMCMC algorithm for a \code{cloe_input} object. Details
#' of the arguments are available in the documentation of
#' \code{\link{sampler_cnn}}.
#' 
#' @param input a \code{cloe_input} object
#' @param method a sampling method. Currently only 'cnn' (copy-number neutral)
#'   is available.
#' @param ... additional method-specific parameters
#' @return a \code{cloe_mcmc} object
#' @seealso \code{\link{cloe_input}} for the input object,
#'   \code{\link{cloe_mcmc}} for the returned object,
#'   \code{\link{sampler_cnn}} for the CNN sampler,
#'   and \code{\link{summarise}} for what to do with the return value
#' @export
sampler <- function(input, method, ...){
  stopifnot("cloe_input" %in% class(input))

  type <- input$get_type()

  if(missing(method)){
    if(type == "tar"){
      return(sampler_cnn(input=input, ...))
    }else{
      stop("Not implemented yet")
    }
  }

  if(method == "cnn" && type == "tar"){
    return(sampler_cnn(input=input, ...))
  }else{
    stop("Method - input type combination is not supported")
  }
}


sampler_cnn_r <- function(input, iterations=10000, verbose=TRUE,
    progress_interval=100, save_all=FALSE,
    K=5, mu=0.3, rho=0.1, nu=0.5, epsilon=0.002,
    fix_z_and_tree=FALSE, z_value, tree_value, fix_f=FALSE, f_value, 
    fix_gammas=FALSE, gamma_values=1, gamma_shape=2, gamma_rate=1,
    fix_s=FALSE, s_value, s_shape=11, s_rate=0.10, theta=0.2,
    chains=4, temperatures=c(1, 0.9, 0.82, 0.75), swap_interval=100,
    f_scaling=300, f_bias=4, mh_gamma_sd=0.6, mh_s_sd=18){
  stopifnot("cloe_input" %in% class(input))
  stopifnot(input$get_type() == "tar")

  iterations        <- as.integer(iterations)
  progress_interval <- as.integer(progress_interval)
  save_all          <- as.logical(save_all)

  chains <- as.integer(chains)
  if(chains < 1L){
    stop("'chains' must be an integer greater than 0")
  }
  # temperatures must be a vector of length 'chains' where the first entry is 1
  # and every other entry is in (0, 1)
  if(chains == 1L){
    temperatures <- 1
  }else{
    if(missing(temperatures) || length(temperatures) != chains){
      stop("'temperatures' must be a vector of length specified by 'chains'")
    }
    if(temperatures[1] != 1 || any(temperatures[2:chains] <= 0) ||
      any(temperatures[2:chains] >= 1)){
      stop("'temperatures' must be a c(1, ...) vector where ... are real ",
           "numbers in (0, 1)")
    }
    swap_interval <- as.integer(swap_interval)
    if(swap_interval < 1 || swap_interval > iterations){
      stop("'swap_interval' must be in [1, iterations].")
    }
  }
  progress_interval <- as.integer(progress_interval)
  if(progress_interval < 1 || progress_interval > iterations){
    stop("'progress_interval' must be in [1, iterations]. To suppress ",
         "progress metrics choose verbose=FALSE")
  }

  # this is a list of all the parameters that change for each chain
  variables <- vector("list", chains)
  for(m in 1:chains){
    variables[[m]] <- list()
  }

  K_set <- FALSE

  J <- input$get_j()
  T <- input$get_t()
  X <- input$get_mutant_reads()
  Y <- input$get_wildtype_reads()

  if(J > 1000L){
    stop("Error: too many mutations")
  }

  # input sanitation
  if(!missing(K)){
    K_set <- TRUE
    K <- as.integer(K)
  }


  mu <- as.numeric(mu)
  if(mu <= 0 || mu >= 1){
    stop("'mu' must be in (0, 1)")
  }
  lmu   <- log(mu)
  lmu1m <- log(1 - mu)
  rho <- as.numeric(rho)
  if(rho < 0 || rho >= 1){
    stop("'rho' must be in [0, 1)")
  }
  if(rho == 0){
    rho <- get("cloe_0_correction", envir=cloe_globals_env)
  }
  lrho   <- log(rho)
  lrho1m <- log(1 - rho)
  isa <- TRUE
  nu <- as.numeric(nu)
  if(nu < 0.5 || nu > 1){
    stop("'nu' must be in [0.5, 1]")
  }
  if(nu == 1) warning("nu = 1 may impede mixing")
  if(nu == 0.5){
    isa <- FALSE
    message("ISA is disabled")
  }
  lnu    <- log(nu)
  lnu1m  <- log(1 - nu)

  epsilon <- as.numeric(epsilon)
  if(epsilon < 0 | epsilon > 1){
    stop("'epsilon' must be in [0, 1], ideally close to 0 since it's an ",
         "error rate")
  }
  if(epsilon == 0) epsilon <- get("cloe_0_correction", envir=cloe_globals_env)
  epsilon1m <- 1 - epsilon

  theta <- as.numeric(theta)
  if(theta <= 0 || theta >= 1){
    stop("'theta' must be in (0, 1)")
  }


  if(!missing(z_value)){
    if(!is.matrix(z_value) || nrow(z_value) != J){
      stop("'z_value' must be a J x K matrix")
    }
    if(K_set){
      if(ncol(z_value) != K){
        stop("'z_value' must have K columns")
      }
    }else{
      K_set <- TRUE
      K <- ncol(z_value)
    }
    if(all(names(table(c(z_value))) != c("0", "1"))){
      stop("'z_value' must only contain 0 and 1")
    }
  }else{
    if(fix_z_and_tree){
      stop("Cannot fix Z if 'z_value' is missing ('fix_z_and_tree' is on)")
    }
  }

  if(!missing(tree_value)){
    if(!is.vector(tree_value)){
      stop("'tree_value' must be a vector of length K")
    }
    if(K_set){
      if(length(tree_value) != K){
        stop("'tree_value' must be a vector of length K")
      }
    }else{
      K_set <- TRUE
      K <- length(tree_value)
    }
    # check that tree_value conforms to our requirements
    tree_value <- as.integer(tree_value)
    if(tree_value[1] != 0 || any(tree_value[-1] <= 0)){
      stop("tree_value[1] must be 0, and no other element may be 0")
    }
    if(any(tree_value >= 1:K)){
      stop("'tree_value' (and consequently 'Z') must be sorted so that each ",
           "entry refers to one of the previous nodes")
    }
  }else{
    if(fix_z_and_tree){
      stop("Cannot fix tree if 'tree_value' is missing ('fix_z_and_tree' is on)")
    }
  }

  if(!missing(f_value)){
    if(!is.matrix(f_value) || ncol(f_value) != T){
      stop("'f_value' must be a K x T matrix")
    }
    if(K_set){
      if(nrow(f_value) != K){
        stop("'f_value' must have K rows")
      }
    }else{
      K_set <- TRUE
      K <- nrow(f_value)
    }
    if(any(is.na(f_value)) || any(f_value < 0) || any(f_value > 1)){
      stop("'f_value' must be a K x T matrix of T stochastic vectors")
    }
    if(any(apply(f_value, 2L, sum) - 1 > 0.00001)){
      stop("Columns in f_value must sum to 1")
    }
  }else{
    if(fix_f){
      stop("Cannot fix F if 'f_value' is missing ('fix_f' is on)") 
    }
  }


  # initialisations
  # Z and tree
  if(fix_z_and_tree){
    for(m in 1:chains){
      variables[[m]][["Z"]]    <- z_value
      variables[[m]][["tree"]] <- tree_value
    }

    names_tmp <- c("np", apply(z_value, 2L, bits2id))
    tree_value_converted <- names_tmp[tree_value + 1L]
    names(tree_value_converted) <- names_tmp[-1]

    message("Running sampler with a fixed Z and tree")
  }else{
    if(missing(tree_value)){
      for(m in 1:chains){
        variables[[m]][["tree"]] <- random_tree(K)
      }
    }else{
      for(m in 1:chains){
        variables[[m]][["tree"]] <- tree_value
      }
    }

    if(missing(z_value)){
      for(m in 1:chains){
        variables[[m]][["Z"]] <- initialise_z(J=J, K=K,
            tree=variables[[m]][["tree"]], mu=mu, rho=rho, nu=nu)
      }
    }else{
      for(m in 1:chains){
        variables[[m]][["Z"]] <- z_value
      }
    }
  }
  if(isa){
    for(m in 1:chains){
      variables[[m]][["ancestors"]] <- vector("list", K)
      variables[[m]][["ancestors"]][[1]] <- 1L
      variables[[m]][["ancestors"]][[2]] <- c(2L, 1L)
      for(k in 3:K){
        variables[[m]][["ancestors"]][[k]] <- ancestors(tree=variables[[m]][["tree"]], node=k)
      }
    }
  }

  # gammas
  if(missing(gamma_values) || is.null(gamma_values)){
    if(fix_gammas){
      stop("Cannot fix gammas if 'gamma_values' is missing ('fix_gammas' is on)")
    }

    for(m in 1:chains){
      # safe initialisation
      variables[[m]][["gammas"]] <- vapply(rgamma(n=T, shape=gamma_shape,
          rate=gamma_rate), FUN=max, FUN.VALUE=0, 1)
    }
  }else{
    if(length(gamma_values) != T || any(is.na(gamma_values)) ||
       any(gamma_values < 0)){
      stop("'gamma_values' must be a vector of length T with non-zero ",
           "positive real numbers")
    }

    for(m in 1:chains){
      variables[[m]][["gammas"]] <- gamma_values
    }
  }

  # F
  if(fix_f){
    for(m in 1:chains){
      variables[[m]][["F"]] <- f_value
    }
    message("Running sampler with a fixed F")
  }else{
    for(m in 1:chains){
      variables[[m]][["F"]] <- initialise_f(K, T, variables[[m]][["gammas"]])
    }
  }

  # we provide a default for s, so s won't be null but it may be missing
  # missing should not be a problem
  if(missing(s_value) || is.null(s_value)){
    if(fix_s){
      stop("Cannot fix s if 's_value' is missing ('fix_s' is on)")
    }

    for(m in 1:chains){
      variables[[m]][["s"]] <- rgamma(1L, s_shape, s_rate)
    }
  }else{
    if(s_value < 0){
      stop("'s_value' must be a positive real number")
    }

    for(m in 1:chains){
      variables[[m]][["s"]] <- s_value
    }
  }


  ## set up traces
  # z and tree
  if(!fix_z_and_tree){
    trace_z    <- vector("list", chains)
    trace_tree <- vector("list", chains)
    if(save_all){
      for(m in 1:chains){
        trace_z[[m]]    <- vector("list", iterations)
        trace_tree[[m]] <- vector("list", iterations)
      }
    }else{
      trace_z[[1]]    <- vector("list", iterations)
      trace_tree[[1]] <- vector("list", iterations)
    }
  }
  # gammas
  if(!fix_gammas){
    trace_gammas <- vector("list", chains)
    if(save_all){
      for(m in 1:chains){
        trace_gammas[[m]] <- vector("list", iterations)
      }
    }else{
      trace_gammas[[1]] <- vector("list", iterations)
    }
  }
  # s
  if(!fix_s){
    trace_s <- vector("list", chains)
    if(save_all){
      for(m in 1:chains){
        trace_s[[m]] <- rep(NA, iterations)
      }
    }else{
      trace_s[[1]] <- rep(NA, iterations)
    }
  }
  # f, loglikelihood
  trace_f <- vector("list", chains)
  trace_ll <- vector("list", chains)
  if(save_all){
    for(m in 1:chains){
      trace_f[[m]] <- vector("list", iterations)
      trace_ll[[m]] <- rep(0, iterations)
    }
  }else{
    trace_f[[1]] <- vector("list", iterations)
    trace_ll[[1]] <- rep(0, iterations)
  }
  # logposterior, always save all of these because they are used for chain swaps
  trace_logposterior <- vector("list", chains)
  names(trace_logposterior) <- paste0("logposterior_", 1:chains)
  for(m in 1:chains){
    trace_logposterior[[m]] <- rep(0, iterations)
  }  

  # mh
  # these cannot be held in 'variables' because those lists are passed from chain to
  # chain, whereas I want the MH/update statistics for each tempered chain, hence:
  update_stats <- list()
  for(m in 1:chains){
    update_stats[[m]] <- list()
    update_stats[[m]][["mh_gammas_trials"]]   <- rep(0L, T)
    update_stats[[m]][["mh_gammas_accepted"]] <- rep(0L, T)
    update_stats[[m]][["mh_f_trials"]]        <- rep(0L, T)
    update_stats[[m]][["mh_f_accepted"]]      <- rep(0L, T)
    update_stats[[m]][["mh_s_trials"]]        <- 0L
    update_stats[[m]][["mh_s_accepted"]]      <- 0L
    update_stats[[m]][["mh_z_trials"]]        <- 0L
    update_stats[[m]][["mh_z_accepted"]]      <- 0L
    update_stats[[m]][["subtree_changes"]]    <- 0L
    update_stats[[m]][["sibling_swaps"]]      <- 0L
    update_stats[[m]][["parent_swaps"]]       <- 0L
  }

  chain_swaps <- list()
  # chain_swaps[[i]] holds the counts of swaps between i and i + 1
  # (only swapping between adjacent chains)
  if(chains > 2L){
    for(m in 1:(chains - 1L)){
      chain_swaps[[m]] <- list(accepted=c(), trials=c())
    }
  }else{
    chain_swaps[[1]] <- list(accepted=c(), trials=c())
  }

  # these are to save repeating operations in later function calls
  lpriors_tree <- vector("list", K)
  for(k in 3:K) lpriors_tree[[k]] <- c(-log(2 * k), rep(log(1 - 0.5 / k) - log(k - 2), k - 2))
  common_ancestors <- K:1
  normal_hist <- bits2id(variables[[1]][["Z"]][, 1])

  # condense data and params
  obs_data <- list(X=X, Y=Y, J=J, T=T)
  params <- list(K=K, lmu=lmu, lmu1m=lmu1m, lrho=lrho, lrho1m=lrho1m, isa=isa,
      lnu=lnu, lnu1m=lnu1m, e=epsilon, e1m=epsilon1m, theta=theta,
      fix_z_and_tree=fix_z_and_tree, fix_f=fix_f, fix_gammas=fix_gammas,
      fix_s=fix_s, f_scaling=f_scaling, f_bias=f_bias, gamma_shape=gamma_shape,
      gamma_rate=gamma_rate, mh_gamma_sd=mh_gamma_sd, s_shape=s_shape,
      s_rate=s_rate, mh_s_sd=mh_s_sd, lpriors_tree=lpriors_tree,
      ca=common_ancestors)

  # keep track of time
  t00 <- t0  <- Sys.time() # start time of sampler and time at the beginning of the progress interval
  t1         <- 0          # time at the end of the progress interval
  t_elapsed  <- 0          # time since the beginning of the MCMC sampler
  t_interval <- 0          # t1-t0

  if(verbose) message("Starting sampler with K=", K, " for ", iterations,
      " iterations")

  for(iteration in 1:iterations){
    ## progress
    if(verbose && iteration %% progress_interval == 0L){
      t1 <- Sys.time()
      t_interval <- difftime(t1, t0, units="secs")
      t_elapsed  <- difftime(t1, t00, units="auto")

      cat(sprintf("iteration %7s  -  %4.2f it/s  -  elapsed %6.2f %1s  -  remaining %6.2f m                    \r",
          iteration,
          progress_interval / as.double(t_interval),
          as.double(t_elapsed),
          substr(units(t_elapsed), 1L, 1L),
          (iterations - iteration) * as.double(t_interval) / progress_interval / 60))
      t0 <- t1
    }


    # scan each chain
    for(m in 1:chains){
      variables[[m]] <- iterate(vars=variables[[m]], obs_data=obs_data,
                                params=params, tau=temperatures[m])

      # update MH/update stats
      update_stats[[m]][["mh_z_accepted"]]      <- update_stats[[m]][["mh_z_accepted"]] +
                                                      variables[[m]][["mh_z_accepted"]]
      update_stats[[m]][["mh_z_trials"]]        <- update_stats[[m]][["mh_z_trials"]] +
                                                      variables[[m]][["mh_z_trials"]]
      update_stats[[m]][["subtree_changes"]]    <- update_stats[[m]][["subtree_changes"]] +
                                                      variables[[m]][["subtree_changes"]]
      update_stats[[m]][["sibling_swaps"]]      <- update_stats[[m]][["sibling_swaps"]] +
                                                      variables[[m]][["sibling_swaps"]]
      update_stats[[m]][["mh_f_accepted"]]      <- update_stats[[m]][["mh_f_accepted"]] +
                                                      variables[[m]][["mh_f_accepted"]]
      update_stats[[m]][["mh_f_trials"]]        <- update_stats[[m]][["mh_f_trials"]] +
                                                      variables[[m]][["mh_f_trials"]]
      update_stats[[m]][["mh_gammas_accepted"]] <- update_stats[[m]][["mh_gammas_accepted"]] +
                                                      variables[[m]][["mh_gammas_accepted"]]
      update_stats[[m]][["mh_gammas_trials"]]   <- update_stats[[m]][["mh_gammas_trials"]] +
                                                      variables[[m]][["mh_gammas_trials"]]
      update_stats[[m]][["mh_s_accepted"]]      <- update_stats[[m]][["mh_s_accepted"]] +
                                                      variables[[m]][["mh_s_accepted"]]
      update_stats[[m]][["mh_s_trials"]]        <- update_stats[[m]][["mh_s_trials"]] +
                                                      variables[[m]][["mh_s_trials"]]
      trace_logposterior[[m]][iteration] <- temperatures[m] *
          lposterior(vars=variables[[m]], obs_data=obs_data, params=params)
    }

    if(chains > 1L && iteration %% swap_interval == 0L){
      # propose swap: chain j with chain j + 1
      j <- sample.int(chains - 1, size=1, replace=FALSE)
      if(log(runif(n=1L, min=0, max=1)) <= min(0,
          # log-posteriors are already tempered, so reverse that if necessary
          temperatures[j + 1] * trace_logposterior[[j]][iteration] / temperatures[j] +
          temperatures[j] * trace_logposterior[[j + 1]][iteration] / temperatures[j + 1] -
          trace_logposterior[[j]][iteration] -
          trace_logposterior[[j + 1]][iteration])){
        # proposed swap has been accepted
        variables[c(j, j + 1L)] <- variables[c(j + 1L, j)]
        # it's good that we're saving variables to traces after the swap, but we've already saved
        # logposterior, so need to swap it now (and retemper it)
        tmp <- trace_logposterior[[j]][iteration] / temperatures[j] * temperatures[j + 1]
        trace_logposterior[[j]][iteration] <- trace_logposterior[[j + 1]][iteration] / temperatures[j + 1] * temperatures[j]
        trace_logposterior[[j + 1]][iteration] <- tmp
        chain_swaps[[j]][["accepted"]] <- append(chain_swaps[[j]][["accepted"]], iteration)
      }
      chain_swaps[[j]][["trials"]] <- append(chain_swaps[[j]][["trials"]], iteration)
    }

    if(save_all){
      # save traces for all chains
      if(!fix_z_and_tree){
        for(m in 1:chains){
          # this_hist      <- apply(variables[[m]][["Z"]], 2L, bits2id)
          this_hist <- c(normal_hist, rep("", K - 1L))
          for(k in 2:K){
            this_hist[k] <- bits2id(variables[[m]][["Z"]][, k])
          }
          this_hist_tree <- c("np", this_hist)
          trace_z[[m]][[iteration]] <- this_hist
          trace_tree[[m]][[iteration]] <- this_hist_tree[variables[[m]][["tree"]] + 1L]
          names(trace_tree[[m]][[iteration]]) <- this_hist
        }
      }
      if(!fix_gammas){
        for(m in 1:chains){
          trace_gammas[[m]][[iteration]] <- variables[[m]][["gammas"]]
        }
      }
      if(!fix_s){
        for(m in 1:chains){
          trace_s[[m]][iteration] <- variables[[m]][["s"]]
        }
      }
      for(m in 1:chains){
        trace_f[[m]][[iteration]] <- variables[[m]][["F"]]
        trace_ll[[m]][iteration] <- temperatures[m] * ll(X=X, Y=Y, Z=variables[[m]][["Z"]],
            F=variables[[m]][["F"]], s=variables[[m]][["s"]], params=params)
      }
    }else{
      if(!fix_z_and_tree){
        this_hist <- c(normal_hist, rep("", K - 1L))
        for(k in 2:K){
          this_hist[k] <- bits2id(variables[[1]][["Z"]][, k])
        }
        this_hist_tree <- c("np", this_hist)
        trace_z[[1]][[iteration]] <- this_hist
        trace_tree[[1]][[iteration]] <- this_hist_tree[variables[[1]][["tree"]] + 1L]
        names(trace_tree[[1]][[iteration]]) <- this_hist
      }
      if(!fix_gammas){
        trace_gammas[[1]][[iteration]] <- variables[[1]][["gammas"]]
      }
      if(!fix_s){
        trace_s[[1]][iteration] <- variables[[1]][["s"]]
      }
      trace_f[[1]][[iteration]] <- variables[[1]][["F"]]
      trace_ll[[1]][iteration] <- temperatures[1] * ll(X=X, Y=Y, Z=variables[[1]][["Z"]],
          F=variables[[1]][["F"]], s=variables[[1]][["s"]], params=params)
    }
  }
  if(verbose) cat("\n")


  # fool the final segment into believing there is only one chain
  if(!save_all) chains <- 1L

  trace_k_effective <- vector("list", chains)
  if(!fix_z_and_tree){
    for(m in 1:chains){
      trace_k_effective[[m]] <- unlist(lapply(trace_z[[m]], function(x) length(unique(x))))
    }
  }else{
    trace_k_effective <- lapply(1:chains, function(x) length(unique(apply(z_value, 2L, bits2id))))
  }

  ret <- vector("list", chains)
  for(m in 1:chains){
    traces <- list(
        loglikelihood=trace_ll[[m]],
        logposterior=trace_logposterior[[m]],
        K=trace_k_effective[[m]],
        Z=if(fix_z_and_tree){ apply(z_value, 2L, bits2id) }else{ trace_z[[m]] },
        F=trace_f[[m]],
        tree=if(fix_z_and_tree){ tree_value_converted }else{ trace_tree[[m]] },
        gammas=if(fix_gammas){ gamma_values }else{ trace_gammas[[m]] },
        s=if(fix_s){ s_value }else{ trace_s[[m]] }
      )

    ret[[m]] <- cloe_mcmc$new(
        input=input,
        type=input$get_type(),
        method="cnn",
        traces=traces,
        params=list(
          iterations=iterations,
          K=K,
          mu=mu,
          rho=rho,
          nu=nu,
          epsilon=epsilon,
          theta=theta,
          F_scale=f_scaling,
          F_bias=f_bias,
          K0=1L, # number of fixed clones (normal)
          Z0=as.matrix(variables[[m]][["Z"]][, 1]),
          fixed=list(
            s=fix_s,
            gammas=fix_gammas,
            tree=fix_z_and_tree,
            Z=fix_z_and_tree,
            F=fix_f
          ),
          tau=temperatures[m],
          MH_gamma_sd=mh_gamma_sd,
          MH_s_sd=mh_s_sd
        ),
        priors=list(
          gammas=list(shape=gamma_shape, rate=gamma_rate),
          s=list(shape=s_shape, rate=s_rate)
        ),
        mh=list(
          Z=list(accepted=update_stats[[m]][["mh_z_accepted"]], trials=update_stats[[m]][["mh_z_trials"]]),
          F=list(accepted=update_stats[[m]][["mh_f_accepted"]], trials=update_stats[[m]][["mh_f_trials"]]),
          gammas=list(accepted=update_stats[[m]][["mh_gammas_accepted"]], trials=update_stats[[m]][["mh_gammas_trials"]]),
          s=list(accepted=update_stats[[m]][["mh_s_accepted"]], trials=update_stats[[m]][["mh_s_trials"]]),
          tree_updates=list(accepted=update_stats[[m]][["subtree_changes"]], trials=K * iterations),
          sibling_swaps=list(accepted=update_stats[[m]][["sibling_swaps"]], trials=iterations),
          parent_swaps=list(accepted=update_stats[[m]][["parent_swaps"]], trials=iterations),
          chain_swaps=list(accepted=sum(sapply(chain_swaps, function(x) length(x[["accepted"]]))),
            trials=sum(sapply(chain_swaps, function(x) length(x[["trials"]]))))
        ),
        swaps=chain_swaps
      )
  }

  if(save_all){
    return(ret)
  }else{
    return(ret[[1]])
  }
}
#' cloe's finite sampler with random phylogeny
#' 
#' This function implements a (Metropolis-coupled) Markov chain Monte Carlo
#' algorithm to obtain samples from the posterior distribution of clonal
#' genotypes, their phylogeny, and clonal fractions, given mutation sequencing
#' data.
#' 
#' Please refer to cloe's paper for more details on the model.
#' 
#' @param input a \code{cloe_input} object
#' @param iterations the number of MCMC iterations to run for
#' @param verbose whether to output messages during the run
#' @param progress_interval interval for printing progress metrics
#' @param K the (maximum) number of clones to consider
#' @param mu mutation probability
#' @param rho reversion probability
#' @param nu ISA factor. A value of 1 means that any transition that violates
#'   ISA has probability 0. A value of 0 means the opposite (and does not make)
#'   much sense. To disable ISA checks, set this to 0.5. Values between 0.5 and
#'   1 penalise transitions that break ISA but do not forbid them.
#' @param epsilon error rate (probability of observing an artefactual
#'   non-reference read)
#' @param fix_z_and_tree whether to fix the genotypes matrix and the
#'   phylogeny. If so, both \code{z_value} and \code{tree_value} must be set.
#' @param z_value a genotypes matrix. Warning: specifying a value here fixes
#'   the Z parameters during inference. Set this if \code{fix_z_and_tree = TRUE}.
#'   The number \code{K} of clones will be set to \code{ncol(z_value)}.
#' @param tree_value a tree vector. Warning: specifying a value here fixes
#'   the tree parametesr during inference. Set this if
#'   \code{fix_z_and_tree = TRUE}.
#' @param fix_f whether to fix the clonal fractions matrix. If so, \code{f_value}
#'   must be set.
#' @param f_value a clonal fractions matrix. Warning: specifying a value here
#'   fixes the F parameters during inference. Set this if \code{fix_f = TRUE}.
#'   The number \code{K} of clones will be set to \code{nrow(f_value)}.
#' @param fix_gammas whether to fix the Dirichlet parameters. If so,
#'   \code{gamma_values} must be set.
#' @param gamma_values numeric vector (of length \code{T}) of values for the
#'   gamma hyperparameters. Set this if \code{fix_gammas = TRUE}.
#' @param gamma_shape the shape parameter of the prior Gamma distribution of
#'   the gammas (symmetric Dirichlet parameters)
#' @param gamma_rate the rate parameter of the prior Gamma distribution of the
#'   gammas (symmetric Dirichlet parameters)
#' @param fix_s whether to fix the Beta-Binomial overdispersion parameter. 
#' @param s_value a numeric value for the Beta-Binomial overdispersion
#'   parameter. This will only be taken into account if \code{fix_s = TRUE}.
#' @param s_shape the shape parameter of the prior Gamma distribution of the
#'   overdispersion parameter
#' @param s_rate the rate parameter of the prior Gamma distribution of the
#'   overdispersion parameter
#' @param theta probability of flipping a genotype in the MH proposal to update
#'   \code{Z}.
#' @param chains the number of parallel chains to run. The higher this number,
#'   the worse the performance. \code{chains=3} or \code{chains=4} should work
#'   well in most cases.
#' @param temperatures the inverse temperature parameters at which to run the
#'   parallel chains. This must be a vector of length \code{chains}, where the
#'   first entry is 1 and the remaining entries are reals in (0, 1). A value of
#'   1 corresponds to the correct posterior distribution. A value of 0
#'   corresponds to a uniform posterior (completely flat).
#' @param swap_interval interval for proposing chain swaps
#' @param f_scaling the scaling factor for the Dirichlet proposal. The new
#'   vector is obtained by the product of \code{f_scaling * old_vector + f_bias}
#' @param f_bias the bias for the Dirichlet proposal. The new vector is
#'   obtained by the product of \code{f_scaling * old_vector + f_bias}
#' @param mh_gamma_sd the standard deviation of the Normal distribution used
#'   to propose new gammas in the Metropolis-Hastings step.
#' @param mh_s_sd the standard deviation of the Normal distribution used to
#'   propose a new overdispersion parameter in the Metropolis-Hastings step.
#' @return a \code{cloe_mcmc} object with all input parameters and traces
#'   for the parameters that need to be inferred
#' @seealso \code{\link{cloe_input}} for the input object,
#'   \code{\link{cloe_mcmc}} for the returned object,
#'   \code{\link{sampler}} for the generic sampler call, and
#'   \code{\link{summarise}} for what to do with the return value
#' @export
sampler_cnn <- compiler::cmpfun(sampler_cnn_r)

