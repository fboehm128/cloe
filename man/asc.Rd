% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/functions.R
\name{asc}
\alias{asc}
\title{ASCII value of character}
\usage{
asc(c)
}
\arguments{
\item{c}{character}
}
\value{
\code{c}'s ASCII value
}
\description{
\code{asc} returns the ASCII value of character \code{c}.
This function is from \link{http://www.r-bloggers.com/ascii-code-table-in-r/}
}
\seealso{
\code{\link{chr}}
}

